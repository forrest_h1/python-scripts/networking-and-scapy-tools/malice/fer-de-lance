# (Fer-)De-Lance

DISCLAIMER: Deauth attacks walk the very fine line between what the FCC considers jamming, and defeating the obnoxious neighbor's phone speaker with an 1800w Stereo. IN MOST CASES, if a deauth attack is discovered and subsequently linked to you by Law Enforcement, there is generally a fine or penalty involved. It is SIGNIFICANTLY more obvious that an attack is occuring if you are broadcasting a frame addressed to all connected clients. 




## About


De-Lance is a Wireless Deauth utility written in Python using a LOT of Scapy. A user can specify a valid Gateway MAC Address for the AP as well as any connected client - at this time, the script does not check to see if this client and AP are actively communicating, but this functionality will be added soon.



What De-Lance and basically all other Deauth clients do is, first, identify a nearby target Station (The Wireless Network), and the target Client(s). Then, a frame/packet is crafted as being sent from the Station, meant for the Client - this Frame specifies that the Client should disconnect, which occurs automatically. Generally, the reason given in these frames is Reason 7: Class 3 frame received from nonassociated STA. If the connection between the Station and Client is not frame protected (802.11w), then for as long as we transmit these deauth frames, the client will either have intermittent connectivity at best or no connection at all. 



If it's not obvious - this can be illegal in certain applications. I created it mostly as a learning tool for myself and for the sake of seeing which devices I owned actually supported 802.11w. Please don't be dumb. You can spoof a frame header, you can't spoof an RSSI reading.



## Module Requirements



Scapy 2.5.0



Validators 0.20.0



Everything is pretty much a built-in according to pip


 
## Usage



At this time, you will have to manually set your wireless mode to managed. v1.4 will support automatic mode switching, but for now: Either run `$ sudo airmon-ng start [YOUR_WIRELESS_INTERFACE]` or `$ sudo ifconfig [IFACE] down && sudo iwconfig [IFACE] mode Monitor && sudo ifconfig [IFACE] up`. I wrote a bash script to actually do this for you awhile ago that you can alias, [WlanX](https://gitlab.com/forrest_h1/bash-scripts/networking-tools/wlanx) that will save you from running a bunch of ifconfig and iwconfig commands.

Once the card is in monitor mode, you don't need to do much - I've written the script to either search for a card matching the way Airmon-NG names mode-switched cards (IFACEXmon) or find any card that's in monitor mode. You can skip this check by using the `-i [CARD]` argument though.



Attack all clients connected to BSSID forever:
```
sudo ./de-lance.py -t bcast -g <AP_MAC> 

#or

sudo ./de-lance.py -t ff:ff:ff -g <AP_MAC>
```



Attack single client connected to BSSID for X amount of frames:
```
sudo ./de-lance.py -t <CLIENT_MAC> -g <AP_MAC> -c <ANY_INTEGER>
```



Specify a card to use for the attack:
```
sudo ./delance.py -i <name_of_card_in_monitor_mode> -t <CLIENT_MAC> -g <AP_MAC>
```



