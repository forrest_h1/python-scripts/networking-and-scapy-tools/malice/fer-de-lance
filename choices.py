#!/usr/bin/env python3

user_flags = {
        "yes": [ "y", "yy", "yes", "ok", "okay" ],
        "no": [ "n", "nn", "no", "nope", "negative" ],
        "cancel": [ "cancel", "exit", "quit", "q", "qq"],
}

loop_flags = [
        "infinite",
        "all",
        "forever",
        "loop",
        "repeat",
        "repeating",
        "r",
        "l",
        "a"
]
