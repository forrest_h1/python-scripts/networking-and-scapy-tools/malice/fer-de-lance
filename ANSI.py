#!/usr/bin/env python3
#
# ANSI.py - Terminal Formatting/Manipulation Library
#
# Written by Forrest Hooker, 05/20/2024 (Ago Consulting)

# Terminal Manipulation #

#Clears current line
CLR = '\033[2K'
#Moves cursor UP one line
U1 = '\033[A'
#Moves cursor DOWN one line
D1 = '\033[B'
#Move cursor to the start of the line
L_START = '\033[1000D'
#Equivalent of '$tput smcup'
ALT_BUFF = '\033[?1049h'
#Equivalent of '$tput rmcup'
RST_BUFF = '\033[?1049l'
#Move to start of line, clear line
L_CLR = (f"{L_START}{CLR}")
#Up 1, Clear Line
U1C = (f"{U1}{CLR}")


# Cursor Manipulation #

#Hide Cursor
HIDE_C = '\033[?25l'
#Show Cursor
SHOW_C = '\033[?25h'

# Text Formatting #

#Resets ALL colors and formatting
R = '\033[0m'
#Bold text
B = '\033[1m'
#Italic text
ITAL = '\033[3m'
#Underlined text
UL = '\033[4m'
#Clear all formatting, retains colors
FCL = '\033[22m\033[23m\033[24m\033[25m'

# Colors #

RED = '\033[31m'
GRN = '\033[32m'
YEL = '\033[33m'
BLU = '\033[34m'
MAG = '\033[35m'
CYN = '\033[36m'
WHT = '\033[37m'
GRA = '\033[90m'

# BOLD Format Combos #

#Bold + Underline
BU = (f"{B}{UL}")
#Bold + Italics
BI = (f"{B}{ITAL}")
#Bold + Red
BR = (f"{B}{RED}")
#Bold + Green
BG = (f"{B}{GRN}")
#Bold + Yellow
BY = (f"{B}{YEL}")
#Bold + Blue 
BB = (f"{B}{BLU}")
#Bold + Magenta
BM = (f"{B}{MAG}")
#Bold + Cyan
BC = (f"{B}{CYN}")
#Bold + White
BW = (f"{B}{WHT}")

# BOLD + UNDERLINE Format Combos #

BUR = (f"{BU}{RED}")
BUG = (f"{BU}{GRN}")
BUY = (f"{BU}{YEL}")
BUB = (f"{BU}{BLU}")
BUM = (f"{BU}{MAG}")
BUC = (f"{BU}{CYN}")
BUW = (f"{BU}{WHT}")


## SYMBOLS ##

CHK = '\u2713'

THINK = [ '', '.', '..', '...' ]

SPIN = '\\|/-'

# STRINGS #

ERR = (f"{BR}ERROR:{FCL} ")


